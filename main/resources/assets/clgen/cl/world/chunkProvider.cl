float interpolate(float f)
{
	return 1.0f - f * f * (3.0f - f * 2.0f);
}

float noise1d(__global const uchar* noiseMap, const uint sizeExponent, float x, const uint octaves, const float smoothness)
{
	const int andField = (1 << sizeExponent) - 1;

	float noise = 0.0f;
	float value = 1.0f;
	float maxValue = 0.0f;

	int ix;
	int ix1;
	float xMix;
	float xMixi;

	for(int i = 0; i < octaves; i++)
	{
		maxValue += value;

		ix = floor(x);
		ix1 = ix + 1;
		xMix = interpolate(x - ix);
		xMixi = 1.0f - xMix;

		ix = ix & andField;
		ix1 = ix1 & andField;

		noise += (noiseMap[ix] * xMix
			+ noiseMap[ix1] * xMixi)
			* value;

		x *= 0.5f;
		value *= smoothness;
	}

	return noise / maxValue;
}

float noise2d(__global const uchar* noiseMap, const uint sizeExponent, float x, float y, const uint octaves, const float smoothness)
{
	const int andField = (1 << sizeExponent) - 1;

	float noise = 0.0f;
	float value = 1.0f;
	float maxValue = 0.0f;

	int ix;
	int ix1;
	int iy;
	int iy1;
	float xMix;
	float xMixi;
	float yMix;
	float yMixi;

	for(int i = 0; i < octaves; i++)
	{
		maxValue += value;

		ix = floor(x);
		ix1 = ix + 1;
		iy = floor(y);
		iy1 = iy + 1;
		xMix = interpolate(x - ix);
		xMixi = 1.0f - xMix;
		yMix = interpolate(y - iy);
		yMixi = 1.0f - yMix;

		ix = (ix & andField) << sizeExponent;
		ix1 = (ix1 & andField) << sizeExponent;
		iy = iy & andField;
		iy1 = iy1 & andField;

		noise += (noiseMap[ix | iy] * xMix * yMix
			+ noiseMap[ix1 | iy] * xMixi * yMix
			+ noiseMap[ix | iy1] * xMix * yMixi
			+ noiseMap[ix1 | iy1] * xMixi * yMixi)
			* value;

		x *= 0.5f;
		y *= 0.5f;
		value *= smoothness;
	}

	return noise / maxValue;
}

float noise3d(global const uchar* noiseMap, const uint sizeExponent, float x, float y, float z, const uint octaves, const float smoothness)
{
	const int andField = (1 << sizeExponent) - 1;

	float noise = 0.0f;
	float value = 1.0f;
	float maxValue = 0.0f;

	int ix;
	int ix1;
	int iy;
	int iy1;
	int iz;
	int iz1;
	float xMix;
	float xMixi;
	float yMix;
	float yMixi;
	float zMix;
	float zMixi;

	for(int i = 0; i < octaves; i++)
	{
		maxValue += value;

		ix = floor(x);
		ix1 = ix + 1;
		iy = floor(y);
		iy1 = iy + 1;
		iz = floor(z);
		iz1 = iz + 1;
		xMix = interpolate(x - ix);
		xMixi = 1.0f - xMix;
		yMix = interpolate(y - iy);
		yMixi = 1.0f - yMix;
		zMix = interpolate(z - iz);
		zMixi = 1.0f - zMix;

		ix = (ix & andField) << (sizeExponent * 2);
		ix1 = (ix1 & andField) << (sizeExponent * 2);
		iy = (iy & andField) << sizeExponent;
		iy1 = (iy1 & andField) << sizeExponent;
		iz = iz & andField;
		iz1 = iz1 & andField;

		noise += (noiseMap[ix | iy | iz] * xMix * yMix * zMix
			+ noiseMap[ix1 | iy | iz] * xMixi * yMix * zMix
			+ noiseMap[ix | iy1 | iz] * xMix * yMixi * zMix
			+ noiseMap[ix1 | iy1 | iz] * xMixi * yMixi * zMix
			+ noiseMap[ix | iy | iz1] * xMix * yMix * zMixi
			+ noiseMap[ix1 | iy | iz1] * xMixi * yMix * zMixi
			+ noiseMap[ix | iy1 | iz1] * xMix * yMixi * zMixi
			+ noiseMap[ix1 | iy1 | iz1] * xMixi * yMixi * zMixi)
			* value;

		x *= 0.5f;
		y *= 0.5f;
		z *= 0.5f;
		value *= smoothness;
	}

	return noise / maxValue;
}

/*float upscale(uint x, uint z, global const float* smoothedField)
{
	const float xMixi = (x & 3) / 4.0f;
	const float xMix = 1.0f - xMixi;
	const float zMixi = (z & 3) / 4.0f;
	const float zMix = 1.0f - zMixi;
	
	x >>= 2;
	z >>= 2;
	
	return smoothedField[x * 5 + z] * xMix * zMix
		+ smoothedField[(x + 1) * 5 + z] * xMixi * zMix
		+ smoothedField[x * 5 + z + 1] * xMix * zMixi
		+ smoothedField[(x + 1) * 5 + z + 1] * xMixi * zMixi;
}*/

bool isCave(const float x, const float y, const float z, global const float* caves, const uint amount)
{
	for(int i = 0; i < amount; i++)
	{
		const float apx = x - caves[i * 8];
		const float apy = y - caves[i * 8 + 1];
		const float apz = z - caves[i * 8 + 2];
		const float abx = caves[i * 8 + 4] - caves[i * 8];
		const float aby = caves[i * 8 + 5] - caves[i * 8 + 1];
		const float abz = caves[i * 8 + 6] - caves[i * 8 + 2];

		const float ab2 = abx * abx + aby * aby + abz * abz;
		const float ap_ab = apx * abx + apy * aby + apz * abz;
		float t = ap_ab / ab2;
		
		if(t < 0.0) t = 0.0;
		else if(t > 1.0) t = 1.0;

		const float r = (1.0 - t) * caves[i * 8 + 3] + t * caves[i * 8 + 7];
		const float pcx = apx - t * abx;
		const float pcy = apy - t * aby;
		const float pcz = apz - t * abz;
		
		if(pcx * pcx + pcy * pcy + pcz * pcz < r * r)
		{
			return true;
		}
	}
	
	return false;
}

kernel void provideChunk(const int chunkX, const int chunkZ, global uchar* blocks, global const uchar* rootHeightNoise, global const uchar* heightVariationNoise, global const uchar* solidNoise, global const float* caves, const uint caveAmount)
{
	const int x = get_global_id(0) | chunkX << 4;
	const int y = get_global_id(1);
	const int z = get_global_id(2) | chunkZ << 4;
	const int ptr = (get_global_id(0) << 4 | get_global_id(2)) << 8 | y;
	
	const float rootHeight = (0.25f + noise2d(rootHeightNoise, 4, x * 8.0f, z * 8.0f, 12, 2.0f) * 0.75f) * 96.0f;
	const float heightVariation = (0.1f + noise2d(heightVariationNoise, 4, x * 8.0f, z * 8.0f, 12, 2.0f) * 0.9f) * 32.0f;
	
	const float treshold = (y - rootHeight - heightVariation) / (heightVariation * 2.0f);
	
	float solid = noise3d(solidNoise, 4, x * 16.0f, y * 16.0f, z * 16.0f, 12, 2.0f);
	if(solid > treshold)
	{
		if(isCave(x, y, z, caves, caveAmount))
		{
			blocks[ptr] = 0;
		}
		else
		{
			blocks[ptr] = 1;
		}
	}
	else if(y < 64)
	{
		blocks[ptr] = 2;
	}
	else
	{
		blocks[ptr] = 0;
	}
	
	//if(y >= 64) blocks[ptr] = 0;
	//else if(y >= 56) blocks[ptr] = 2;
	//else blocks[ptr] = 1;
}

kernel void createBiomeMap(const int chunkX, const int chunkZ, global uchar* biomeMap, const int biomeAmount, global const float* biomeApproximationValues, global const uchar* rootHeightNoise, global const uchar* heightVariationNoise, global const uchar* temperatureNoise, global const uchar* humidityNoise)
{
	const int x = get_global_id(0) | chunkX << 4;
	const int z = get_global_id(1) | chunkZ << 4;
	
	const float rootHeight = noise2d(rootHeightNoise, 4, x * 8.0f, z * 8.0f, 12, 2.0f);
	const float heightVariation = noise2d(heightVariationNoise, 4, x * 8.0f, z * 8.0f, 12, 2.0f);
	const float temperature = noise2d(rootHeightNoise, 4, x * 2.0f, z * 2.0f, 12, 2.5f);
	const float humidity = noise2d(heightVariationNoise, 4, x * 2.0f, z * 2.0f, 12, 2.5f);
	
	uchar indexSmallest = 0;
	float smallestDistance = 0.0f;
	for(uchar index = 0; index < biomeAmount * 4; index += 4)
	{
		float dRootHeight = biomeApproximationValues[index] - rootHeight;
		float dHeightVariation = biomeApproximationValues[index + 1] - heightVariation;
		float dTemperature = biomeApproximationValues[index + 2] - temperature;
		float dHumidity = biomeApproximationValues[index + 3] - humidity;
		float distance = dRootHeight * dRootHeight * 4.0f + dHeightVariation * dHeightVariation * 4.0f + dTemperature * dTemperature + dHumidity * dHumidity;
		//float distance = dRootHeight * dRootHeight;
		if(index == 0 || distance < smallestDistance)
		{
			indexSmallest = index / 4;
			smallestDistance = distance;
		}
	}
	
	biomeMap[get_global_id(1) << 4 | get_global_id(0)] = indexSmallest;
}

/*kernel void smooth(const int smoothingSize, global const float* smoothingField, global const float* rootHeight, global const float* heightVariation, global float* smoothRootHeight, global float* smoothHeightVariation)
{
	float rootHeightVal;
	float heightVariationVal;
	float maxVal;
	
	for(int x = -smoothingSize; x <= smoothingSize; x++)
	{
		for(int z = -smoothingSize; z <= smoothingSize; z++)
		{
			const float power = smoothingField[(x + smoothingSize) * (smoothingSize * 2 + 1) + z + smoothingSize];
			const int largePtr = (get_global_id(0) + smoothingSize + x) * (5 + smoothingSize * 2) + get_global_id(1) + smoothingSize + z;
			
			rootHeightVal += power * rootHeight[largePtr];
			heightVariationVal += power * heightVariation[largePtr];
			maxVal += power;
		}
	}
	
	const int smallPtr = get_global_id(1) * 5 + get_global_id(0);
	smoothRootHeight[smallPtr] = (rootHeightVal / maxVal + 2.0f) / 4.0f;
	smoothHeightVariation[smallPtr] = heightVariationVal / maxVal;
}*/