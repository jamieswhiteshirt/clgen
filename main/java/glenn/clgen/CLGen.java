package glenn.clgen;

import java.nio.FloatBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.PointerBuffer;
import org.lwjgl.opencl.CL10;
import org.lwjgl.opencl.CLCommandQueue;
import org.lwjgl.opencl.CLKernel;
import org.lwjgl.opencl.CLMem;
import org.lwjgl.opencl.CLProgram;

import glenn.clgen.cl.CLCore;
import glenn.clgen.common.Configurations;
import glenn.clgen.common.WorldTypeCLGen;
import net.minecraft.init.Blocks;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.WorldType;
import net.minecraftforge.common.config.Configuration;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerAboutToStartEvent;
import cpw.mods.fml.common.event.FMLServerStartedEvent;
import cpw.mods.fml.common.event.FMLServerStoppedEvent;

@Mod(modid = CLGen.MODID, version = CLGen.VERSION)
public class CLGen
{
	public static final String MODID = "CLGen";
	public static final String VERSION = "1.0";
	
	public static WorldType clGenWorldType;
	
	public static Configurations config;
	
	@EventHandler
	public void preInit(FMLPreInitializationEvent event)
	{
		config = new Configurations();
		config.init(event.getSuggestedConfigurationFile());
		
		
		
		/*float[] a = new float[]{4.0F, 1.0F, 3.0F, 2.0F};
		FloatBuffer aBuf = BufferUtils.createFloatBuffer(a.length);
		aBuf.put(a);
		aBuf.rewind();
		float[] b = new float[]{0.0F, 3.0F, 1.0F, 2.0F};
		FloatBuffer bBuf = BufferUtils.createFloatBuffer(b.length);
		bBuf.put(b);
		bBuf.rewind();
		FloatBuffer cBuf = BufferUtils.createFloatBuffer(4);

		CLMem aMem = CLCore.getMem("testA", CL10.CL_MEM_READ_ONLY | CL10.CL_MEM_COPY_HOST_PTR, aBuf);
		CLMem bMem = CLCore.getMem("testB", CL10.CL_MEM_READ_ONLY | CL10.CL_MEM_COPY_HOST_PTR, bBuf);
		CLMem cMem = CLCore.getMem("testC", CL10.CL_MEM_WRITE_ONLY, cBuf);
		
		PointerBuffer globalWS = BufferUtils.createPointerBuffer(1);
		globalWS.put(0, aBuf.capacity());
		
		CLProgram program = CLCore.getProgram(new ResourceLocation("clgen:cl/world/thing.cl"));
		CLKernel kernel = CLCore.getKernel("sum", program);
		
		kernel.setArg(0, aMem);
		kernel.setArg(1, bMem);
		kernel.setArg(2, cMem);
		
		CLCommandQueue queue = CLCore.getCommandQueue("test");
		
		CL10.clEnqueueNDRangeKernel(queue, kernel, 1, null, globalWS, null, null, null);
		CL10.clEnqueueReadBuffer(queue, cMem, 1, 0, cBuf, null, null);
		CL10.clFinish(queue);
		
		float[] c = new float[4];
		for(int i = 0; i < cBuf.capacity(); i++)
		{
			c[i] = cBuf.get(i);
		}*/
	}

	@EventHandler
	public void load(FMLInitializationEvent event)
	{
		clGenWorldType = new WorldTypeCLGen("CLGen");
	}

	@EventHandler
	public void postInit(FMLPostInitializationEvent event)
	{
		
	}
	
	@EventHandler
	public void serverStart(FMLServerAboutToStartEvent event)
	{
		CLCore.init();
	}
	
	@EventHandler
	public void serverStop(FMLServerStoppedEvent event)
	{
		CLCore.cleanup();
	}
}
