package glenn.clgen.cl;

import org.lwjgl.opencl.CL10;

public enum DeviceType
{
	ACCELLERATOR(CL10.CL_DEVICE_TYPE_ACCELERATOR),
	ALL(CL10.CL_DEVICE_TYPE_ALL),
	CPU(CL10.CL_DEVICE_TYPE_CPU),
	DEFAULT(CL10.CL_DEVICE_TYPE_DEFAULT),
	GPU(CL10.CL_DEVICE_TYPE_GPU);
	
	public int cap;
	
	DeviceType(int cap)
	{
		this.cap = cap;
	}
}