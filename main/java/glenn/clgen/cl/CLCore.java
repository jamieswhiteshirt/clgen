package glenn.clgen.cl;

import glenn.clgen.CLGen;

import java.io.InputStream;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.LongBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.IResourceManager;
import net.minecraft.util.ResourceLocation;

import org.apache.commons.io.IOUtils;
import org.lwjgl.opencl.CL;
import org.lwjgl.opencl.CL10;
import org.lwjgl.opencl.CLCommandQueue;
import org.lwjgl.opencl.CLContext;
import org.lwjgl.opencl.CLDevice;
import org.lwjgl.opencl.CLKernel;
import org.lwjgl.opencl.CLMem;
import org.lwjgl.opencl.CLPlatform;
import org.lwjgl.opencl.CLProgram;
import org.lwjgl.opencl.OpenCLException;
import org.lwjgl.opencl.Util;

public class CLCore
{
	private static class MemKey
	{
		public String name;
		public int flags;
		
		public MemKey(String name, int flags)
		{
			this.name = name;
			this.flags = flags;
		}
	}
	
	private static class KernelKey
	{
		public String name;
		public CLProgram program;
		
		public KernelKey(String name, CLProgram program)
		{
			this.name = name;
			this.program = program;
		}
	}
	
	private static CLPlatform platform;
	private static List<CLDevice> devices;
	private static CLContext context;
	
	private static Map<KernelKey, CLKernel> kernels;
	private static Map<ResourceLocation, CLProgram> programs;
	private static Map<MemKey, CLMem> mems;
	private static Map<String, CLCommandQueue> commandQueues;
	
	public static boolean init()
	{
		try
		{
			CL.create();
			
			platform = CLPlatform.getPlatforms().get(CLGen.config.platformIndex);
			devices = platform.getDevices(CLGen.config.deviceType.cap);
			context = CLContext.create(platform, devices, null, null, null);
			
			kernels = new HashMap<KernelKey, CLKernel>();
			programs = new HashMap<ResourceLocation, CLProgram>();
			mems = new HashMap<MemKey, CLMem>();
			commandQueues = new HashMap<String, CLCommandQueue>();
			
			return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean cleanup()
	{
		for(CLKernel kernel : kernels.values())
		{
			if(kernel != null) CL10.clReleaseKernel(kernel);
		}
		kernels = null;
		
		for(CLProgram program : programs.values())
		{
			if(program != null) CL10.clReleaseProgram(program);
		}
		programs = null;
		
		for(CLMem mem : mems.values())
		{
			if(mem != null) CL10.clReleaseMemObject(mem);
		}
		mems = null;
		
		for(CLCommandQueue commandQueue : commandQueues.values())
		{
			if(commandQueue != null) CL10.clReleaseCommandQueue(commandQueue);
		}
		commandQueues = null;
		
		if(context != null)
		{
			CL10.clReleaseContext(context);
			context = null;
		}
		
		CL.destroy();
		
		return true;
	}
	
	public static CLCommandQueue getCommandQueue(String name)
	{
		CLCommandQueue queue = commandQueues.get(name);
		if(queue == null)
		{
			queue = CL10.clCreateCommandQueue(context, devices.get(CLGen.config.deviceIndex), CL10.CL_QUEUE_PROFILING_ENABLE, null);
			commandQueues.put(name, queue);
		}
		return queue;
	}
	
	public static CLProgram getProgram(ResourceLocation resourceLocation)
	{
		CLProgram program = programs.get(resourceLocation);
		
		if(program == null)
		{
			String programString = null;
			try
			{
				IResourceManager resourceManager = Minecraft.getMinecraft().getResourceManager();
				InputStream stream = resourceManager.getResource(resourceLocation).getInputStream();
				programString = IOUtils.toString(stream, "UTF-8");
			}
			catch(Exception e)
			{
				throw new RuntimeException("Unable to load OpenCL program.", e);
			}
			
			program = CL10.clCreateProgramWithSource(context, programString, null);
			try
			{
				Util.checkCLError(CL10.clBuildProgram(program, devices.get(CLGen.config.deviceIndex), "", null));
			}
			catch(OpenCLException e)
			{
				throw new RuntimeException("OpenCL program build error:\n" + program.getBuildInfoString(devices.get(CLGen.config.deviceIndex), CL10.CL_PROGRAM_BUILD_LOG));
			}
		}
		
		return program;
	}
	
	public static CLKernel getKernel(String name, CLProgram program)
	{
		KernelKey key = new KernelKey(name, program);
		CLKernel kernel = kernels.get(key);
		if(kernel == null)
		{
			kernel = CL10.clCreateKernel(program, name, null);
			kernels.put(key, kernel);
		}
		return kernel;
	}
	
	public static CLMem getMem(String name, int flags, ByteBuffer buffer)
	{
		MemKey key = new MemKey(name, flags);
		CLMem mem = mems.get(key);
		if(mem == null)
		{
			mem = CL10.clCreateBuffer(context, flags, buffer, null);
			mems.put(key, mem);
		}
		return mem;
	}
	
	public static CLMem getMem(String name, int flags, DoubleBuffer buffer)
	{
		MemKey key = new MemKey(name, flags);
		CLMem mem = mems.get(key);
		if(mem == null)
		{
			mem = CL10.clCreateBuffer(context, flags, buffer, null);
			mems.put(key, mem);
		}
		return mem;
	}
	
	public static CLMem getMem(String name, int flags, FloatBuffer buffer)
	{
		MemKey key = new MemKey(name, flags);
		CLMem mem = mems.get(key);
		if(mem == null)
		{
			mem = CL10.clCreateBuffer(context, flags, buffer, null);
			mems.put(key, mem);
		}
		return mem;
	}
	
	public static CLMem getMem(String name, int flags, IntBuffer buffer)
	{
		MemKey key = new MemKey(name, flags);
		CLMem mem = mems.get(key);
		if(mem == null)
		{
			mem = CL10.clCreateBuffer(context, flags, buffer, null);
			mems.put(key, mem);
		}
		return mem;
	}
	
	public static CLMem getMem(String name, int flags, long hostSize)
	{
		MemKey key = new MemKey(name, flags);
		CLMem mem = mems.get(key);
		if(mem == null)
		{
			mem = CL10.clCreateBuffer(context, flags, hostSize, null);
			mems.put(key, mem);
		}
		return mem;
	}
	
	public static CLMem getMem(String name, int flags, LongBuffer buffer)
	{
		MemKey key = new MemKey(name, flags);
		CLMem mem = mems.get(key);
		if(mem == null)
		{
			mem = CL10.clCreateBuffer(context, flags, buffer, null);
			mems.put(key, mem);
		}
		return mem;
	}
	
	public static CLMem getMem(String name, int flags, ShortBuffer buffer)
	{
		MemKey key = new MemKey(name, flags);
		CLMem mem = mems.get(key);
		if(mem == null)
		{
			mem = CL10.clCreateBuffer(context, flags, buffer, null);
			mems.put(key, mem);
		}
		return mem;
	}
}