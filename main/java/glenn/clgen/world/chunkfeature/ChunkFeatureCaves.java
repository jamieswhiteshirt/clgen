package glenn.clgen.world.chunkfeature;

import glenn.clgen.noise.Perlin1D;

import java.nio.FloatBuffer;
import java.util.Arrays;
import java.util.Random;

import net.minecraft.util.MathHelper;

public class ChunkFeatureCaves extends ChunkFeature
{
	private class CaveNode
	{
		public float x;
		public float y;
		public float z;
		public float w;
		
		public CaveNode(float x, float y, float z, float w)
		{
			this.x = x;
			this.y = y;
			this.z = z;
			this.w = w;
		}
	}
	
	public ChunkFeatureCaves(long worldSeed)
	{
		super(worldSeed);
	}

	@Override
	protected int getAffectionRange()
	{
		return 3;
	}

	@Override
	protected float[] getFeatureInChunk(int chunkX, int chunkZ)
	{
		float[] feature = new float[4 * 40 * 8 * 2];
		int featureSize = 0;
		
		Random rand = new Random((long)chunkX * 341873128712L + (long)chunkZ * 132897987541L + worldSeed);
		
		final int amountOfTries = 4;
		final int chancePerTry = 2;
		final float noiseOffset = 20.0F;
		
		for(int i = 0; i < amountOfTries; i++)
		{
			if(rand.nextInt(chancePerTry) == 0)
			{
				final int length = rand.nextInt(20) + 20;
				
				Perlin1D xNoise = new Perlin1D(rand, 4);
				Perlin1D yNoise = new Perlin1D(rand, 4);
				Perlin1D zNoise = new Perlin1D(rand, 4);
				Perlin1D wNoise = new Perlin1D(rand, 4);
				
				CaveNode prevNode = new CaveNode((chunkX + rand.nextFloat()) * 16.0f, rand.nextFloat() * 128.0f, (chunkZ + rand.nextFloat()) * 16.0f, 0.0f);
				
				for(int j = 0; j < length; j++)
				{
					float tX = xNoise.get(noiseOffset + j * 16.0f, 6, 1.5f) - 0.5f;
					float tY = yNoise.get(noiseOffset + j * 16.0f, 6, 1.5f) - 0.5f;
					float tZ = zNoise.get(noiseOffset + j * 16.0f, 6, 1.5f) - 0.5f;
					float tW = wNoise.get(noiseOffset + j * 16.0f, 6, 1.5f) - 0.5f;
					
					float d = MathHelper.sqrt_float(tX * tX + tY * tY + tZ * tZ) / 4.0F;
					
					CaveNode newNode = new CaveNode(prevNode.x + tX / d, prevNode.y + tY / d, prevNode.z + tZ / d, 8.0f * (1.0F + tW) * j * (length - j) / (length * length) + 1.0F);
					
					feature[featureSize++] = prevNode.x;
					feature[featureSize++] = prevNode.y;
					feature[featureSize++] = prevNode.z;
					feature[featureSize++] = prevNode.w;
					feature[featureSize++] = newNode.x;
					feature[featureSize++] = newNode.y;
					feature[featureSize++] = newNode.z;
					feature[featureSize++] = newNode.w;
					
					prevNode = newNode;
				}
			}
		}
		
		/*feature[featureSize++] = chunkX * 16.0F + 20.0F;
		feature[featureSize++] = 64.0F;
		feature[featureSize++] = chunkZ * 16.0F + 20.0F;
		feature[featureSize++] = 4.0F;
		feature[featureSize++] = chunkX * 16.0F + 28.0F;
		feature[featureSize++] = 64.0F;
		feature[featureSize++] = chunkZ * 16.0F + 28.0F;
		feature[featureSize++] = 4.0F;
		
		feature[featureSize++] = chunkX * 16.0F - 4.0F;
		feature[featureSize++] = 64.0F;
		feature[featureSize++] = chunkZ * 16.0F + 20.0F;
		feature[featureSize++] = 4.0F;
		feature[featureSize++] = chunkX * 16.0F - 12.0F;
		feature[featureSize++] = 64.0F;
		feature[featureSize++] = chunkZ * 16.0F + 28.0F;
		feature[featureSize++] = 4.0F;*/
		
		return Arrays.copyOf(feature, featureSize);
	}

	@Override
	protected void decorateFloatBufferWithFeature(FloatBuffer buffer, float[] feature, int chunkX, int chunkZ)
	{
		float chunkMinX = chunkX * 16.0F;
		float chunkMaxX = (chunkX + 1) * 16.0F;
		float chunkMinZ = chunkZ * 16.0F;
		float chunkMaxZ = (chunkZ + 1) * 16.0F;
		
		for(int i = 0; i < feature.length; i += 8)
		{
			float minX = lowest(feature[i] - feature[i + 3], feature[i + 4] - feature[i + 7]);
			float maxX = greatest(feature[i] + feature[i + 3], feature[i + 4] + feature[i + 7]);
			float minZ = lowest(feature[i + 2] - feature[i + 3], feature[i + 6] - feature[i + 7]);
			float maxZ = greatest(feature[i + 2] + feature[i + 3], feature[i + 6] + feature[i + 7]);
			
			if(minX <= chunkMaxX && maxX >= chunkMinX && minZ <= chunkMaxZ && maxZ >= chunkMinZ)
			{
				buffer.put(feature, i, 8);
			}
		}
	}
	
	private float lowest(float a, float b)
	{
		return a < b ? a : b;
	}
	
	private float greatest(float a, float b)
	{
		return a > b ? a : b;
	}
}