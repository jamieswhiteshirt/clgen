package glenn.clgen.world.chunkfeature;

import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.HashMap;

public abstract class ChunkFeature
{
	public class ChunkPosition
	{
		public int x;
		public int z;
		
		public ChunkPosition(int x, int z)
		{
			this.x = x;
			this.z = z;
		}
	}
	
	protected HashMap<ChunkPosition, float[]> featureMap;
	protected long worldSeed;
	
	public ChunkFeature(long worldSeed)
	{
		featureMap = new HashMap<ChunkPosition, float[]>();
		this.worldSeed = worldSeed;
	}
	
	public final int getFeaturesForChunk(FloatBuffer buffer, int chunkX, int chunkZ)
	{
		for(int x = -getAffectionRange(); x <= getAffectionRange(); x++)
		{
			for(int z = -getAffectionRange(); z <= getAffectionRange(); z++)
			{
				ChunkPosition pos = new ChunkPosition(chunkX + x, chunkZ + z);
				float[] feature = featureMap.get(pos);
				
				if(feature == null)
				{
					feature = getFeatureInChunk(pos.x, pos.z);
					featureMap.put(pos, feature);
				}
				
				decorateFloatBufferWithFeature(buffer, feature, chunkX, chunkZ);
			}
		}
		
		return buffer.position() / 8;
	}
	
	protected abstract int getAffectionRange();
	
	protected abstract float[] getFeatureInChunk(int chunkX, int chunkZ);
	
	protected abstract void decorateFloatBufferWithFeature(FloatBuffer buffer, float[] feature, int chunkX, int chunkZ);
}