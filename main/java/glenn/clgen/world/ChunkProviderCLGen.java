package glenn.clgen.world;

import static net.minecraftforge.event.terraingen.InitMapGenEvent.EventType.CAVE;
import static net.minecraftforge.event.terraingen.InitMapGenEvent.EventType.MINESHAFT;
import static net.minecraftforge.event.terraingen.InitMapGenEvent.EventType.RAVINE;
import static net.minecraftforge.event.terraingen.InitMapGenEvent.EventType.SCATTERED_FEATURE;
import static net.minecraftforge.event.terraingen.InitMapGenEvent.EventType.STRONGHOLD;
import static net.minecraftforge.event.terraingen.InitMapGenEvent.EventType.VILLAGE;
import static net.minecraftforge.event.terraingen.PopulateChunkEvent.Populate.EventType.ANIMALS;
import static net.minecraftforge.event.terraingen.PopulateChunkEvent.Populate.EventType.DUNGEON;
import static net.minecraftforge.event.terraingen.PopulateChunkEvent.Populate.EventType.ICE;
import static net.minecraftforge.event.terraingen.PopulateChunkEvent.Populate.EventType.LAKE;
import static net.minecraftforge.event.terraingen.PopulateChunkEvent.Populate.EventType.LAVA;
import glenn.clgen.cl.CLCore;
import glenn.clgen.world.chunkfeature.ChunkFeatureCaves;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.lwjgl.BufferUtils;
import org.lwjgl.PointerBuffer;
import org.lwjgl.opencl.CL10;
import org.lwjgl.opencl.CLCommandQueue;
import org.lwjgl.opencl.CLKernel;
import org.lwjgl.opencl.CLMem;
import org.lwjgl.opencl.CLProgram;

import cpw.mods.fml.common.eventhandler.Event.Result;
import net.minecraft.block.Block;
import net.minecraft.block.BlockFalling;
import net.minecraft.entity.EnumCreatureType;
import net.minecraft.init.Blocks;
import net.minecraft.util.IProgressUpdate;
import net.minecraft.util.MathHelper;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.ChunkPosition;
import net.minecraft.world.SpawnerAnimals;
import net.minecraft.world.World;
import net.minecraft.world.WorldType;
import net.minecraft.world.biome.BiomeGenBase;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.MapGenBase;
import net.minecraft.world.gen.MapGenCaves;
import net.minecraft.world.gen.MapGenRavine;
import net.minecraft.world.gen.NoiseGenerator;
import net.minecraft.world.gen.NoiseGeneratorOctaves;
import net.minecraft.world.gen.NoiseGeneratorPerlin;
import net.minecraft.world.gen.feature.WorldGenDungeons;
import net.minecraft.world.gen.feature.WorldGenLakes;
import net.minecraft.world.gen.structure.MapGenMineshaft;
import net.minecraft.world.gen.structure.MapGenScatteredFeature;
import net.minecraft.world.gen.structure.MapGenStronghold;
import net.minecraft.world.gen.structure.MapGenVillage;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.terraingen.ChunkProviderEvent;
import net.minecraftforge.event.terraingen.PopulateChunkEvent;
import net.minecraftforge.event.terraingen.TerrainGen;

public class ChunkProviderCLGen implements IChunkProvider
{
	/** RNG. */
	private Random rand;
	private NoiseGeneratorOctaves field_147431_j;
	private NoiseGeneratorOctaves field_147432_k;
	private NoiseGeneratorOctaves field_147429_l;
	private NoiseGeneratorPerlin field_147430_m;
	/** A NoiseGeneratorOctaves used in generating terrain */
	public NoiseGeneratorOctaves noiseGen5;
	/** A NoiseGeneratorOctaves used in generating terrain */
	public NoiseGeneratorOctaves noiseGen6;
	public NoiseGeneratorOctaves mobSpawnerNoise;
	/** Reference to the World object. */
	private World worldObj;
	/** are map structures going to be generated (e.g. strongholds) */
	private final boolean mapFeaturesEnabled;
	private WorldType field_147435_p;
	private final double[] field_147434_q;
	private double[] stoneNoise = new double[256];
	/** Holds Stronghold Generator */
	private MapGenStronghold strongholdGenerator = new MapGenStronghold();
	/** Holds Village Generator */
	private MapGenVillage villageGenerator = new MapGenVillage();
	/** Holds Mineshaft Generator */
	private MapGenMineshaft mineshaftGenerator = new MapGenMineshaft();
	private MapGenScatteredFeature scatteredFeatureGenerator = new MapGenScatteredFeature();
	/** Holds ravine generator */
	private MapGenBase ravineGenerator = new MapGenRavine();
	/** The biomes that are used to generate the chunk */
	private BiomeGenBase[] biomesForGeneration;
	double[] field_147427_d;
	double[] field_147428_e;
	double[] field_147425_f;
	double[] field_147426_g;
	int[][] field_73219_j = new int[32][32];
	
	protected int chunkGenerationHeight = 256;
	protected ByteBuffer chunkBiomesBuf;
	protected ByteBuffer chunkBlocksBuf;
	protected PointerBuffer globalWSCreateBiomeMap;
	protected PointerBuffer globalWSProvideChunk;
	
	/*protected FloatBuffer smoothingFieldBuf;
	protected FloatBuffer rootHeightBuf;
	protected FloatBuffer heightVariationBuf;
	protected FloatBuffer smoothRootHeightBuf;
	protected FloatBuffer smoothHeightVariationBuf;*/
	
	protected ByteBuffer solidNoise;
	protected ByteBuffer rootHeightNoise;
	protected ByteBuffer heightVariationNoise;
	protected ByteBuffer temperatureNoise;
	protected ByteBuffer humidityNoise;
	
	protected Block[] blockTranslations;
	
	protected BiomeGenBase[] biomes;
	protected FloatBuffer biomeApproximationValuesBuf;
	
	protected ChunkFeatureCaves caves;
	protected FloatBuffer caveBuf;
	
	//protected int biomeHeightSmoothingSize = 2;
	
	{
		strongholdGenerator = (MapGenStronghold) TerrainGen.getModdedMapGen(strongholdGenerator, STRONGHOLD);
		villageGenerator = (MapGenVillage) TerrainGen.getModdedMapGen(villageGenerator, VILLAGE);
		mineshaftGenerator = (MapGenMineshaft) TerrainGen.getModdedMapGen(mineshaftGenerator, MINESHAFT);
		scatteredFeatureGenerator = (MapGenScatteredFeature) TerrainGen.getModdedMapGen(scatteredFeatureGenerator, SCATTERED_FEATURE);
		ravineGenerator = TerrainGen.getModdedMapGen(ravineGenerator, RAVINE);
	}
	
	public ChunkProviderCLGen(World world, long worldSeed, boolean generateStructures, String worldOptions)
	{
		this.worldObj = world;
		this.mapFeaturesEnabled = generateStructures;
		this.field_147435_p = world.getWorldInfo().getTerrainType();
		this.rand = new Random(worldSeed);
		this.field_147431_j = new NoiseGeneratorOctaves(this.rand, 16);
		this.field_147432_k = new NoiseGeneratorOctaves(this.rand, 16);
		this.field_147429_l = new NoiseGeneratorOctaves(this.rand, 8);
		this.field_147430_m = new NoiseGeneratorPerlin(this.rand, 4);
		this.noiseGen5 = new NoiseGeneratorOctaves(this.rand, 10);
		this.noiseGen6 = new NoiseGeneratorOctaves(this.rand, 16);
		this.mobSpawnerNoise = new NoiseGeneratorOctaves(this.rand, 8);
		this.field_147434_q = new double[825];
		
		
		
		NoiseGenerator[] noiseGens = {field_147431_j, field_147432_k, field_147429_l, field_147430_m, noiseGen5, noiseGen6, mobSpawnerNoise};
		noiseGens = TerrainGen.getModdedNoiseGenerators(world, this.rand, noiseGens);
		this.field_147431_j = (NoiseGeneratorOctaves)noiseGens[0];
		this.field_147432_k = (NoiseGeneratorOctaves)noiseGens[1];
		this.field_147429_l = (NoiseGeneratorOctaves)noiseGens[2];
		this.field_147430_m = (NoiseGeneratorPerlin)noiseGens[3];
		this.noiseGen5 = (NoiseGeneratorOctaves)noiseGens[4];
		this.noiseGen6 = (NoiseGeneratorOctaves)noiseGens[5];
		this.mobSpawnerNoise = (NoiseGeneratorOctaves)noiseGens[6];
		
		chunkBiomesBuf = BufferUtils.createByteBuffer(256);
		chunkBlocksBuf = BufferUtils.createByteBuffer(65536);
		
		globalWSCreateBiomeMap = createPointerBuffer(new long[]{16, 16});
		globalWSProvideChunk = createPointerBuffer(new long[]{16, chunkGenerationHeight, 16});
		
		/*smoothingFieldBuf = BufferUtils.createFloatBuffer((biomeHeightSmoothingSize * 2 + 1) * (biomeHeightSmoothingSize * 2 + 1));
		for(int j = -biomeHeightSmoothingSize; j <= biomeHeightSmoothingSize; ++j)
		{
			for(int k = -biomeHeightSmoothingSize; k <= biomeHeightSmoothingSize; ++k)
			{
				float f = 1.0F / MathHelper.sqrt_float(j * j + k * k + 0.2F);
				if(f < 0.0F) f = 0.0F;
				smoothingFieldBuf.put(f);
			}
		}
		smoothingFieldBuf.rewind();
		
		rootHeightBuf = BufferUtils.createFloatBuffer((biomeHeightSmoothingSize * 2 + 5) * (biomeHeightSmoothingSize * 2 + 5));
		heightVariationBuf = BufferUtils.createFloatBuffer((biomeHeightSmoothingSize * 2 + 5) * (biomeHeightSmoothingSize * 2 + 5));
		smoothRootHeightBuf = BufferUtils.createFloatBuffer(25);
		smoothHeightVariationBuf = BufferUtils.createFloatBuffer(25);*/
		
		solidNoise = createNoiseBuffer(4096);
		rootHeightNoise = createNoiseBuffer(256);
		heightVariationNoise = createNoiseBuffer(256);
		temperatureNoise = createNoiseBuffer(256);
		humidityNoise = createNoiseBuffer(256);
		
		blockTranslations = new Block[256];
		blockTranslations[1] = Blocks.stone;
		blockTranslations[2] = Blocks.water;
		
		ArrayList<BiomeGenBase> biomeList = new ArrayList<BiomeGenBase>();
		for(BiomeGenBase biome : BiomeGenBase.getBiomeGenArray())
		{
			if(biome != null && biome != BiomeGenBase.hell && !biome.biomeName.toLowerCase().contains("river"))
			{
				biomeList.add(biome);
			}
		}
		
		biomes = new BiomeGenBase[]{
				BiomeGenBase.desert,
				BiomeGenBase.forest,
				BiomeGenBase.extremeHills,
				BiomeGenBase.swampland,
				BiomeGenBase.plains,
				BiomeGenBase.taiga,
				BiomeGenBase.desert,
				BiomeGenBase.savanna,
				BiomeGenBase.plains,
				BiomeGenBase.mushroomIsland,
				BiomeGenBase.mesaPlateau,
				BiomeGenBase.mesaPlateau_F,
				BiomeGenBase.jungle,
				BiomeGenBase.megaTaiga,
				BiomeGenBase.mushroomIsland,
				BiomeGenBase.mushroomIslandShore,
				BiomeGenBase.ocean,
				BiomeGenBase.beach,
				BiomeGenBase.deepOcean,
				BiomeGenBase.stoneBeach,
				BiomeGenBase.desertHills,
				BiomeGenBase.forestHills,
				BiomeGenBase.birchForestHills,
				BiomeGenBase.roofedForest
		};
		//biomes = new BiomeGenBase[biomeList.size()];
		/*biomes = new BiomeGenBase[]{
				BiomeGenBase.desert,
				BiomeGenBase.extremeHills
		};*/
		biomeList.toArray(biomes);
		biomeApproximationValuesBuf = BufferUtils.createFloatBuffer(biomes.length * 4);
		for(BiomeGenBase biome : biomes)
		{
			biomeApproximationValuesBuf.put(new float[]{(biome.rootHeight + 2.0f) / 4.0f, biome.heightVariation, biome.temperature, biome.rainfall});
		}
		biomeApproximationValuesBuf.rewind();
		
		caves = new ChunkFeatureCaves(worldSeed);
		caveBuf = BufferUtils.createFloatBuffer(2048);
	}
	
	protected PointerBuffer createPointerBuffer(long[] array)
	{
		PointerBuffer buffer = BufferUtils.createPointerBuffer(array.length);
		buffer.put(array);
		buffer.rewind();
		return buffer;
	}
	
	protected ByteBuffer createNoiseBuffer(int size)
	{
		ByteBuffer buffer = BufferUtils.createByteBuffer(size);
		for(int i = 0; i < size; i++)
		{
			buffer.put((byte)(rand.nextBoolean() ? 1 : 0));
		}
		buffer.rewind();
		return buffer;
	}
	
	public void func_147424_a(int chunkX, int chunkZ, Block[] blocks)
	{
		/*this.biomesForGeneration = this.worldObj.getWorldChunkManager().getBiomesForGeneration(null, chunkX * 4 - biomeHeightSmoothingSize, chunkZ * 4 - biomeHeightSmoothingSize, 5 + biomeHeightSmoothingSize * 2, 5 + biomeHeightSmoothingSize * 2);
		
		for(int i = 0; i < biomesForGeneration.length; i++)
		{
			//biomesForGeneration[i] = BiomeGenBase.desert;
			BiomeGenBase biome = biomesForGeneration[i];
			rootHeightBuf.put(biome.rootHeight);
			heightVariationBuf.put(biome.heightVariation);
		}
		rootHeightBuf.rewind();
		heightVariationBuf.rewind();*/
		
		CLCommandQueue queue = CLCore.getCommandQueue("CLGenChunkProvider");
		CLProgram program = CLCore.getProgram(new ResourceLocation("clgen:cl/world/chunkProvider.cl"));
		
		/*CLMem smoothingFieldMem = CLCore.getMem("CLGenSmoothingField", CL10.CL_MEM_READ_ONLY | CL10.CL_MEM_COPY_HOST_PTR, smoothingFieldBuf);
		CLMem rootHeightMem = CLCore.getMem("CLGenRootHeight", CL10.CL_MEM_READ_ONLY, rootHeightBuf);
		CLMem heightVariationMem = CLCore.getMem("CLGenHeightVariation", CL10.CL_MEM_READ_ONLY, heightVariationBuf);
		CLMem smoothRootHeightMem = CLCore.getMem("CLGenSmoothRootHeight", CL10.CL_MEM_READ_WRITE, smoothRootHeightBuf);
		CLMem smoothHeightVariationMem = CLCore.getMem("CLGenSmoothHeightVariation", CL10.CL_MEM_READ_WRITE, smoothHeightVariationBuf);
		
		CLKernel kernelSmooth = CLCore.getKernel("smooth", program);
		kernelSmooth.setArg(0, biomeHeightSmoothingSize);
		kernelSmooth.setArg(1, smoothingFieldMem);
		kernelSmooth.setArg(2, rootHeightMem);
		kernelSmooth.setArg(3, heightVariationMem);
		kernelSmooth.setArg(4, smoothRootHeightMem);
		kernelSmooth.setArg(5, smoothHeightVariationMem);
		CL10.clEnqueueWriteBuffer(queue, rootHeightMem, 1, 0, rootHeightBuf, null, null);
		CL10.clEnqueueWriteBuffer(queue, heightVariationMem, 1, 0, heightVariationBuf, null, null);
		CL10.clFinish(queue);
		CL10.clEnqueueNDRangeKernel(queue, kernelSmooth, 2, null, globalWSSmooth, null, null, null);
		CL10.clEnqueueReadBuffer(queue, smoothRootHeightMem, 1, 0, smoothRootHeightBuf, null, null);
		CL10.clFinish(queue);*/
		
		int amountOfCaves = caves.getFeaturesForChunk(caveBuf, chunkX, chunkZ);
		caveBuf.rewind();
		
		CLMem chunkBiomesMem = CLCore.getMem("CLGenChunkBiomes", CL10.CL_MEM_READ_WRITE, chunkBiomesBuf);
		CLMem biomeApproximationValuesMem = CLCore.getMem("CLGenBiomeApproximationValues", CL10.CL_MEM_READ_ONLY | CL10.CL_MEM_COPY_HOST_PTR, biomeApproximationValuesBuf);
		CLMem chunkBlocksMem = CLCore.getMem("CLGenChunkBlocks", CL10.CL_MEM_WRITE_ONLY, chunkBlocksBuf);
		CLMem solidNoiseMem = CLCore.getMem("CLGenSolidNoise", CL10.CL_MEM_READ_ONLY | CL10.CL_MEM_COPY_HOST_PTR, solidNoise);
		CLMem rootHeightNoiseMem = CLCore.getMem("CLGenRootHeightNoise", CL10.CL_MEM_READ_ONLY | CL10.CL_MEM_COPY_HOST_PTR, rootHeightNoise);
		CLMem heightVariationNoiseMem = CLCore.getMem("CLGenHeightVariationNoise", CL10.CL_MEM_READ_ONLY | CL10.CL_MEM_COPY_HOST_PTR, heightVariationNoise);
		CLMem temperatureNoiseMem = CLCore.getMem("CLGenTemperatureNoise", CL10.CL_MEM_READ_ONLY | CL10.CL_MEM_COPY_HOST_PTR, temperatureNoise);
		CLMem humidityNoiseMem = CLCore.getMem("CLGenHumidityNoise", CL10.CL_MEM_READ_ONLY | CL10.CL_MEM_COPY_HOST_PTR, humidityNoise);
		CLMem caveMem = CLCore.getMem("CLGenCave", CL10.CL_MEM_READ_ONLY, caveBuf);
		
		CLKernel kernelCreateBiomeMap = CLCore.getKernel("createBiomeMap", program);
		CLKernel kernelProvideChunk = CLCore.getKernel("provideChunk", program);
		
		kernelCreateBiomeMap.setArg(0, chunkX);
		kernelCreateBiomeMap.setArg(1, chunkZ);
		kernelCreateBiomeMap.setArg(2, chunkBiomesMem);
		kernelCreateBiomeMap.setArg(3, biomes.length);
		kernelCreateBiomeMap.setArg(4, biomeApproximationValuesMem);
		kernelCreateBiomeMap.setArg(5, rootHeightNoiseMem);
		kernelCreateBiomeMap.setArg(6, heightVariationNoiseMem);
		kernelCreateBiomeMap.setArg(7, temperatureNoiseMem);
		kernelCreateBiomeMap.setArg(8, humidityNoiseMem);
		CL10.clEnqueueNDRangeKernel(queue, kernelCreateBiomeMap, 2, null, globalWSCreateBiomeMap, null, null, null);
		
		CL10.clEnqueueWriteBuffer(queue, caveMem, 1, 0, caveBuf, null, null);
		CL10.clEnqueueReadBuffer(queue, caveMem, 1, 0, caveBuf, null, null);
		CL10.clFinish(queue);
		
		kernelProvideChunk.setArg(0, chunkX);
		kernelProvideChunk.setArg(1, chunkZ);
		kernelProvideChunk.setArg(2, chunkBlocksMem);
		kernelProvideChunk.setArg(3, rootHeightNoiseMem);
		kernelProvideChunk.setArg(4, heightVariationNoiseMem);
		kernelProvideChunk.setArg(5, solidNoiseMem);
		kernelProvideChunk.setArg(6, caveMem);
		kernelProvideChunk.setArg(7, amountOfCaves);
		CL10.clEnqueueNDRangeKernel(queue, kernelProvideChunk, 3, null, globalWSProvideChunk, null, null, null);
		
		CL10.clEnqueueReadBuffer(queue, chunkBlocksMem, 1, 0, chunkBlocksBuf, null, null);
		CL10.clEnqueueReadBuffer(queue, chunkBiomesMem, 1, 0, chunkBiomesBuf, null, null);
		CL10.clFinish(queue);
		
		for(int i = 0; i < 65536; i++)
		{
			blocks[i] = blockTranslations[chunkBlocksBuf.get(i)];
		}
		
		biomesForGeneration = new BiomeGenBase[256];
		for(int i = 0; i < 256; i++)
		{
			biomesForGeneration[i] = biomes[chunkBiomesBuf.get(i)];
		}
	}
	
	public void replaceBlocksForBiome(int p_147422_1_, int p_147422_2_, Block[] p_147422_3_, byte[] p_147422_4_, BiomeGenBase[] p_147422_5_)
	{
		ChunkProviderEvent.ReplaceBiomeBlocks event = new ChunkProviderEvent.ReplaceBiomeBlocks(this, p_147422_1_, p_147422_2_, p_147422_3_, p_147422_4_, p_147422_5_, this.worldObj);
		MinecraftForge.EVENT_BUS.post(event);
		if (event.getResult() == Result.DENY) return;
	
		double d0 = 0.03125D;
		this.stoneNoise = this.field_147430_m.func_151599_a(this.stoneNoise, (double)(p_147422_1_ * 16), (double)(p_147422_2_ * 16), 16, 16, d0 * 2.0D, d0 * 2.0D, 1.0D);
	
		for (int k = 0; k < 16; ++k)
		{
			for (int l = 0; l < 16; ++l)
			{
				BiomeGenBase biomegenbase = p_147422_5_[l + k * 16];
				biomegenbase.genTerrainBlocks(this.worldObj, this.rand, p_147422_3_, p_147422_4_, p_147422_1_ * 16 + k, p_147422_2_ * 16 + l, this.stoneNoise[l + k * 16]);
			}
		}
	}
	
	/**
	 * loads or generates the chunk at the chunk location specified
	 */
	public Chunk loadChunk(int p_73158_1_, int p_73158_2_)
	{
		return this.provideChunk(p_73158_1_, p_73158_2_);
	}
	
	/**
	 * Will return back a chunk, if it doesn't exist and its not a MP client it will generates all the blocks for the
	 * specified chunk from the map seed and chunk seed
	 */
	public Chunk provideChunk(int p_73154_1_, int p_73154_2_)
	{
		this.rand.setSeed((long)p_73154_1_ * 341873128712L + (long)p_73154_2_ * 132897987541L);
		Block[] ablock = new Block[65536];
		byte[] abyte = new byte[65536];
		this.func_147424_a(p_73154_1_, p_73154_2_, ablock);
		//this.biomesForGeneration = this.worldObj.getWorldChunkManager().loadBlockGeneratorData(this.biomesForGeneration, p_73154_1_ * 16, p_73154_2_ * 16, 16, 16);
		this.replaceBlocksForBiome(p_73154_1_, p_73154_2_, ablock, abyte, this.biomesForGeneration);
		this.ravineGenerator.func_151539_a(this, this.worldObj, p_73154_1_, p_73154_2_, ablock);
	
		if (this.mapFeaturesEnabled)
		{
			this.mineshaftGenerator.func_151539_a(this, this.worldObj, p_73154_1_, p_73154_2_, ablock);
			this.villageGenerator.func_151539_a(this, this.worldObj, p_73154_1_, p_73154_2_, ablock);
			this.strongholdGenerator.func_151539_a(this, this.worldObj, p_73154_1_, p_73154_2_, ablock);
			this.scatteredFeatureGenerator.func_151539_a(this, this.worldObj, p_73154_1_, p_73154_2_, ablock);
		}
	
		Chunk chunk = new Chunk(this.worldObj, ablock, abyte, p_73154_1_, p_73154_2_);
		byte[] abyte1 = chunk.getBiomeArray();
	
		for (int k = 0; k < abyte1.length; ++k)
		{
			abyte1[k] = (byte)this.biomesForGeneration[k].biomeID;
		}
	
		chunk.generateSkylightMap();
		return chunk;
	}
	
	/*private void func_147423_a(int p_147423_1_, int p_147423_2_, int p_147423_3_)
	{
		double d0 = 684.412D;
		double d1 = 684.412D;
		double d2 = 512.0D;
		double d3 = 512.0D;
		this.field_147426_g = this.noiseGen6.generateNoiseOctaves(this.field_147426_g, p_147423_1_, p_147423_3_, 5, 5, 200.0D, 200.0D, 0.5D);
		this.field_147427_d = this.field_147429_l.generateNoiseOctaves(this.field_147427_d, p_147423_1_, p_147423_2_, p_147423_3_, 5, 33, 5, 8.555150000000001D, 4.277575000000001D, 8.555150000000001D);
		this.field_147428_e = this.field_147431_j.generateNoiseOctaves(this.field_147428_e, p_147423_1_, p_147423_2_, p_147423_3_, 5, 33, 5, 684.412D, 684.412D, 684.412D);
		this.field_147425_f = this.field_147432_k.generateNoiseOctaves(this.field_147425_f, p_147423_1_, p_147423_2_, p_147423_3_, 5, 33, 5, 684.412D, 684.412D, 684.412D);
		boolean flag1 = false;
		boolean flag = false;
		int l = 0;
		int i1 = 0;
		double d4 = 8.5D;
	
		for (int j1 = 0; j1 < 5; ++j1)
		{
			for (int k1 = 0; k1 < 5; ++k1)
			{
				float f = 0.0F;
				float f1 = 0.0F;
				float f2 = 0.0F;
				byte b0 = 2;
				BiomeGenBase biomegenbase = this.biomesForGeneration[j1 + 2 + (k1 + 2) * 10];
	
				for (int l1 = -b0; l1 <= b0; ++l1)
				{
					for (int i2 = -b0; i2 <= b0; ++i2)
					{
						BiomeGenBase biomegenbase1 = this.biomesForGeneration[j1 + l1 + 2 + (k1 + i2 + 2) * 10];
						float f3 = biomegenbase1.rootHeight;
						float f4 = biomegenbase1.heightVariation;
	
						if (this.field_147435_p == WorldType.AMPLIFIED && f3 > 0.0F)
						{
							f3 = 1.0F + f3 * 2.0F;
							f4 = 1.0F + f4 * 4.0F;
						}
	
						float f5 = this.parabolicField[l1 + 2 + (i2 + 2) * 5] / (f3 + 2.0F);
	
						if (biomegenbase1.rootHeight > biomegenbase.rootHeight)
						{
							f5 /= 2.0F;
						}
	
						f += f4 * f5;
						f1 += f3 * f5;
						f2 += f5;
					}
				}
	
				f /= f2;
				f1 /= f2;
				f = f * 0.9F + 0.1F;
				f1 = (f1 * 4.0F - 1.0F) / 8.0F;
				double d12 = this.field_147426_g[i1] / 8000.0D;
	
				if (d12 < 0.0D)
				{
					d12 = -d12 * 0.3D;
				}
	
				d12 = d12 * 3.0D - 2.0D;
	
				if (d12 < 0.0D)
				{
					d12 /= 2.0D;
	
					if (d12 < -1.0D)
					{
						d12 = -1.0D;
					}
	
					d12 /= 1.4D;
					d12 /= 2.0D;
				}
				else
				{
					if (d12 > 1.0D)
					{
						d12 = 1.0D;
					}
	
					d12 /= 8.0D;
				}
	
				++i1;
				double d13 = (double)f1;
				double d14 = (double)f;
				d13 += d12 * 0.2D;
				d13 = d13 * 8.5D / 8.0D;
				double d5 = 8.5D + d13 * 4.0D;
	
				for (int j2 = 0; j2 < 33; ++j2)
				{
					double d6 = ((double)j2 - d5) * 12.0D * 128.0D / 256.0D / d14;
	
					if (d6 < 0.0D)
					{
						d6 *= 4.0D;
					}
	
					double d7 = this.field_147428_e[l] / 512.0D;
					double d8 = this.field_147425_f[l] / 512.0D;
					double d9 = (this.field_147427_d[l] / 10.0D + 1.0D) / 2.0D;
					double d10 = MathHelper.denormalizeClamp(d7, d8, d9) - d6;
	
					if (j2 > 29)
					{
						double d11 = (double)((float)(j2 - 29) / 3.0F);
						d10 = d10 * (1.0D - d11) + -10.0D * d11;
					}
	
					this.field_147434_q[l] = d10;
					++l;
				}
			}
		}
	}*/
	
	/**
	 * Checks to see if a chunk exists at x, y
	 */
	public boolean chunkExists(int p_73149_1_, int p_73149_2_)
	{
		return true;
	}
	
	/**
	 * Populates chunk with ores etc etc
	 */
	public void populate(IChunkProvider p_73153_1_, int p_73153_2_, int p_73153_3_)
	{
		BlockFalling.fallInstantly = true;
		int k = p_73153_2_ * 16;
		int l = p_73153_3_ * 16;
		BiomeGenBase biomegenbase = this.worldObj.getBiomeGenForCoords(k + 16, l + 16);
		this.rand.setSeed(this.worldObj.getSeed());
		long i1 = this.rand.nextLong() / 2L * 2L + 1L;
		long j1 = this.rand.nextLong() / 2L * 2L + 1L;
		this.rand.setSeed((long)p_73153_2_ * i1 + (long)p_73153_3_ * j1 ^ this.worldObj.getSeed());
		boolean flag = false;
	
		MinecraftForge.EVENT_BUS.post(new PopulateChunkEvent.Pre(p_73153_1_, worldObj, rand, p_73153_2_, p_73153_3_, flag));
	
		if (this.mapFeaturesEnabled)
		{
			this.mineshaftGenerator.generateStructuresInChunk(this.worldObj, this.rand, p_73153_2_, p_73153_3_);
			flag = this.villageGenerator.generateStructuresInChunk(this.worldObj, this.rand, p_73153_2_, p_73153_3_);
			this.strongholdGenerator.generateStructuresInChunk(this.worldObj, this.rand, p_73153_2_, p_73153_3_);
			this.scatteredFeatureGenerator.generateStructuresInChunk(this.worldObj, this.rand, p_73153_2_, p_73153_3_);
		}
	
		int k1;
		int l1;
		int i2;
	
		if (biomegenbase != BiomeGenBase.desert && biomegenbase != BiomeGenBase.desertHills && !flag && this.rand.nextInt(4) == 0
			&& TerrainGen.populate(p_73153_1_, worldObj, rand, p_73153_2_, p_73153_3_, flag, LAKE))
		{
			k1 = k + this.rand.nextInt(16) + 8;
			l1 = this.rand.nextInt(256);
			i2 = l + this.rand.nextInt(16) + 8;
			(new WorldGenLakes(Blocks.water)).generate(this.worldObj, this.rand, k1, l1, i2);
		}
	
		if (TerrainGen.populate(p_73153_1_, worldObj, rand, p_73153_2_, p_73153_3_, flag, LAVA) && !flag && this.rand.nextInt(8) == 0)
		{
			k1 = k + this.rand.nextInt(16) + 8;
			l1 = this.rand.nextInt(this.rand.nextInt(248) + 8);
			i2 = l + this.rand.nextInt(16) + 8;
	
			if (l1 < 63 || this.rand.nextInt(10) == 0)
			{
				(new WorldGenLakes(Blocks.lava)).generate(this.worldObj, this.rand, k1, l1, i2);
			}
		}
	
		boolean doGen = TerrainGen.populate(p_73153_1_, worldObj, rand, p_73153_2_, p_73153_3_, flag, DUNGEON);
		for (k1 = 0; doGen && k1 < 8; ++k1)
		{
			l1 = k + this.rand.nextInt(16) + 8;
			i2 = this.rand.nextInt(256);
			int j2 = l + this.rand.nextInt(16) + 8;
			(new WorldGenDungeons()).generate(this.worldObj, this.rand, l1, i2, j2);
		}
	
		biomegenbase.decorate(this.worldObj, this.rand, k, l);
		if (TerrainGen.populate(p_73153_1_, worldObj, rand, p_73153_2_, p_73153_3_, flag, ANIMALS))
		{
		SpawnerAnimals.performWorldGenSpawning(this.worldObj, biomegenbase, k + 8, l + 8, 16, 16, this.rand);
		}
		k += 8;
		l += 8;
	
		doGen = TerrainGen.populate(p_73153_1_, worldObj, rand, p_73153_2_, p_73153_3_, flag, ICE);
		for (k1 = 0; doGen && k1 < 16; ++k1)
		{
			for (l1 = 0; l1 < 16; ++l1)
			{
				i2 = this.worldObj.getPrecipitationHeight(k + k1, l + l1);
	
				if (this.worldObj.isBlockFreezable(k1 + k, i2 - 1, l1 + l))
				{
					this.worldObj.setBlock(k1 + k, i2 - 1, l1 + l, Blocks.ice, 0, 2);
				}
	
				if (this.worldObj.func_147478_e(k1 + k, i2, l1 + l, true))
				{
					this.worldObj.setBlock(k1 + k, i2, l1 + l, Blocks.snow_layer, 0, 2);
				}
			}
		}
	
		MinecraftForge.EVENT_BUS.post(new PopulateChunkEvent.Post(p_73153_1_, worldObj, rand, p_73153_2_, p_73153_3_, flag));
	
		BlockFalling.fallInstantly = false;
	}
	
	/**
	 * Two modes of operation: if passed true, save all Chunks in one go.  If passed false, save up to two chunks.
	 * Return true if all chunks have been saved.
	 */
	public boolean saveChunks(boolean p_73151_1_, IProgressUpdate p_73151_2_)
	{
		return true;
	}
	
	/**
	 * Save extra data not associated with any Chunk.  Not saved during autosave, only during world unload.  Currently
	 * unimplemented.
	 */
	public void saveExtraData() {}
	
	/**
	 * Unloads chunks that are marked to be unloaded. This is not guaranteed to unload every such chunk.
	 */
	public boolean unloadQueuedChunks()
	{
		return false;
	}
	
	/**
	 * Returns if the IChunkProvider supports saving.
	 */
	public boolean canSave()
	{
		return true;
	}
	
	/**
	 * Converts the instance data to a readable string.
	 */
	public String makeString()
	{
		return "RandomLevelSource";
	}
	
	/**
	 * Returns a list of creatures of the specified type that can spawn at the given location.
	 */
	public List getPossibleCreatures(EnumCreatureType p_73155_1_, int p_73155_2_, int p_73155_3_, int p_73155_4_)
	{
		BiomeGenBase biomegenbase = this.worldObj.getBiomeGenForCoords(p_73155_2_, p_73155_4_);
		return p_73155_1_ == EnumCreatureType.monster && this.scatteredFeatureGenerator.func_143030_a(p_73155_2_, p_73155_3_, p_73155_4_) ? this.scatteredFeatureGenerator.getScatteredFeatureSpawnList() : biomegenbase.getSpawnableList(p_73155_1_);
	}
	
	public ChunkPosition func_147416_a(World p_147416_1_, String p_147416_2_, int p_147416_3_, int p_147416_4_, int p_147416_5_)
	{
		return "Stronghold".equals(p_147416_2_) && this.strongholdGenerator != null ? this.strongholdGenerator.func_151545_a(p_147416_1_, p_147416_3_, p_147416_4_, p_147416_5_) : null;
	}
	
	public int getLoadedChunkCount()
	{
		return 0;
	}
	
	public void recreateStructures(int p_82695_1_, int p_82695_2_)
	{
		if (this.mapFeaturesEnabled)
		{
			this.mineshaftGenerator.func_151539_a(this, this.worldObj, p_82695_1_, p_82695_2_, (Block[])null);
			this.villageGenerator.func_151539_a(this, this.worldObj, p_82695_1_, p_82695_2_, (Block[])null);
			this.strongholdGenerator.func_151539_a(this, this.worldObj, p_82695_1_, p_82695_2_, (Block[])null);
			this.scatteredFeatureGenerator.func_151539_a(this, this.worldObj, p_82695_1_, p_82695_2_, (Block[])null);
		}
	}
}