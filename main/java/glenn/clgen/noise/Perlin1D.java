package glenn.clgen.noise;

import java.util.Random;

import net.minecraft.util.MathHelper;

public class Perlin1D
{
	private final byte[] noiseMap;
	private final int sizeExponent;
	
	public Perlin1D(Random rand, int sizeExponent)
	{
		this.sizeExponent = sizeExponent;
		
		noiseMap = new byte[1 << sizeExponent];
		
		for(int i = 0; i < noiseMap.length; i++)
		{
			noiseMap[i] = (byte)rand.nextInt(2);
		}
	}
	
	public float get(float x, int octaves, float smoothness)
	{
		int andField = (1 << sizeExponent) - 1;
		
		float noise = 0.0f;
		float value = 1.0f;
		float maxValue = 0.0f;

		int ix;
		int ix1;
		float xMix;
		float xMixi;

		for(int i = 0; i < octaves; i++)
		{
			maxValue += value;

			ix = MathHelper.floor_float(x);
			ix1 = ix + 1;
			xMix = interpolate(x - ix);
			xMixi = 1.0f - xMix;

			noise += (noiseMap[ix & andField] * xMix
				+ noiseMap[ix1 & andField] * xMixi)
				* value;

			x *= 0.5f;
			value *= smoothness;
		}

		return noise / maxValue;
	}
	
	private float interpolate(float f)
	{
		return 1.0f - f * f * (3 - f * 2);
	}
}