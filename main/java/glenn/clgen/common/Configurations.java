package glenn.clgen.common;

import glenn.clgen.cl.DeviceType;
import glenn.clgen.common.ConfigurationsBase.TypeConfigurator;

import java.io.File;
import java.util.Map;

public class Configurations extends ConfigurationsBase
{
	@ConfigurationField(name = "Platform index", category = "OpenCL", comment = "The index of the platform OpenCL will use.")
	public int platformIndex = 0;
	
	@ConfigurationField(name = "Device type", category = "OpenCL", comment = "The type of hardware OpenCL will use.")
	public DeviceType deviceType = DeviceType.DEFAULT;
	
	@ConfigurationField(name = "Device index", category = "OpenCL", comment = "The index of the device OpenCL will use.")
	public int deviceIndex = 0;
	
	protected void applyTypeConfigurators(Map<Class<?>, TypeConfigurator> typeConfigurators)
	{
		
	}
}