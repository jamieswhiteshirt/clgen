package glenn.clgen.common;

import java.io.File;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import scala.actors.threadpool.Arrays;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.config.Configuration;

/**
 * An abstract class that uses java reflection to automate the configuration setting and getting.
 * Each contained field must have a default value set statically.
 * @author Glenn
 *
 */
public abstract class ConfigurationsBase
{
	@Retention(RetentionPolicy.RUNTIME)
	protected @interface ConfigurationField
	{
		String name();
		String category() default "Main";
		String comment() default "";
		int iMin() default Integer.MIN_VALUE;
		int iMax() default Integer.MAX_VALUE;
		float fMin() default Float.MIN_VALUE;
		float fMax() default Float.MAX_VALUE;
		String[] validValues() default {};
	}
	
	protected abstract class TypeConfigurator
	{
		public abstract Object apply(Configuration config, Object value, ConfigurationField annotation);
	}
	
	private class TypeConfiguratorInteger extends TypeConfigurator
	{
		public Object apply(Configuration config, Object value, ConfigurationField annotation)
		{
			int def = (Integer)value;
			return new Integer(config.getInt(annotation.name(), annotation.category(), def, annotation.iMin(), annotation.iMax(), annotation.comment()));
		}
	}
	
	private class TypeConfiguratorFloat extends TypeConfigurator
	{
		public Object apply(Configuration config, Object value, ConfigurationField annotation)
		{
			float def = (Float)value;
			return new Float(config.getFloat(annotation.name(), annotation.category(), def, annotation.fMin(), annotation.fMax(), annotation.comment()));
		}
	}
	
	private class TypeConfiguratorDouble extends TypeConfigurator
	{
		public Object apply(Configuration config, Object value, ConfigurationField annotation)
		{
			double def = (Double)value;
			return new Float(config.getFloat(annotation.name(), annotation.category(), (float)def, annotation.fMin(), annotation.fMax(), annotation.comment()));
		}
	}
	
	private class TypeConfiguratorBoolean extends TypeConfigurator
	{
		public Object apply(Configuration config, Object value, ConfigurationField annotation)
		{
			boolean def = (Boolean)value;
			return new Boolean(config.getBoolean(annotation.name(), annotation.category(), def, annotation.comment()));
		}
	}
	
	private class TypeConfiguratorString extends TypeConfigurator
	{
		public Object apply(Configuration config, Object value, ConfigurationField annotation)
		{
			String def = (String)value;
			if(annotation.validValues().length > 0)
			{
				return config.getString(annotation.name(), annotation.category(), def, annotation.comment(), annotation.validValues());
			}
			else
			{
				return config.getString(annotation.name(), annotation.category(), def, annotation.comment());
			}
		}
	}
	
	private class TypeConfiguratorStringArray extends TypeConfigurator
	{
		public Object apply(Configuration config, Object value, ConfigurationField annotation)
		{
			String[] def = (String[])value;
			if(annotation.validValues().length > 0)
			{
				return config.getStringList(annotation.name(), annotation.category(), def, annotation.comment(), annotation.validValues());
			}
			else
			{
				return config.getStringList(annotation.name(), annotation.category(), def, annotation.comment());
			}
		}
	}
	
	private Map<Class<?>, TypeConfigurator> typeConfigurators = new HashMap<Class<?>, TypeConfigurator>();
	
	public ConfigurationsBase()
	{
		typeConfigurators.put(Integer.class, new TypeConfiguratorInteger());
		typeConfigurators.put(Float.class, new TypeConfiguratorFloat());
		typeConfigurators.put(Double.class, new TypeConfiguratorDouble());
		typeConfigurators.put(Boolean.class, new TypeConfiguratorBoolean());
		typeConfigurators.put(String.class, new TypeConfiguratorString());
		typeConfigurators.put(String[].class, new TypeConfiguratorStringArray());
		applyTypeConfigurators(typeConfigurators);
	}
	
	protected void applyTypeConfigurators(Map<Class<?>, TypeConfigurator> typeConfigurators)
	{
		
	}
	
	public final void init(File configurationsFile)
	{
		Configuration config = new Configuration(configurationsFile);
		config.load();
		
		for(Field field : getClass().getFields())
		{
			for(ConfigurationField annotation : field.getAnnotationsByType(ConfigurationField.class))
			{
				try
				{
					Object value = field.get(this);
					Class<?> valueClass = value.getClass();
					TypeConfigurator typeConfigurator = typeConfigurators.get(valueClass);
					
					if(typeConfigurator != null)
					{
						field.set(this, typeConfigurator.apply(config, value, annotation));
					}
					else if(valueClass.isEnum())
					{
						//String enumConstantName = field.set(this, config.getString(annotation.name(), annotation.category(), defaultValue, comment));
						String defaultEnumConstantName = null;
						
						ArrayList<String> enumConstantNames = new ArrayList<String>();
						Map<String, Object> enumConstants = new HashMap<String, Object>();
						
						for(Field enumField : valueClass.getFields())
						{
							if(enumField.isEnumConstant())
							{
								enumConstantNames.add(enumField.getName());
								
								Object o = enumField.get(null);
								enumConstants.put(enumField.getName(), o);
								if(value.equals(o)) defaultEnumConstantName = enumField.getName();
							}
						}
						
						String[] validValues = new String[enumConstantNames.size()];
						enumConstantNames.toArray(validValues);
						String enumConstantName = config.getString(annotation.name(), annotation.category(), defaultEnumConstantName, annotation.comment(), validValues);
						field.set(this, enumConstants.get(enumConstantName));
					}
					else
					{
						System.out.print("Cannot configure for class " + valueClass.getCanonicalName() + " for configuration field " + annotation.category() + " : " + annotation.name());
					}
				}
				catch (Exception e)
				{
					System.out.print("Failed to set value for configuration field " + annotation.category() + " : " + annotation.name());
				}
			}
		}
		
		config.save();
	}
}