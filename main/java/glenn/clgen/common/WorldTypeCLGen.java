package glenn.clgen.common;

import glenn.clgen.gui.GuiCreateCLGenWorld;
import glenn.clgen.world.ChunkProviderCLGen;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiCreateFlatWorld;
import net.minecraft.client.gui.GuiCreateWorld;
import net.minecraft.world.World;
import net.minecraft.world.WorldType;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.ChunkProviderFlat;
import net.minecraft.world.gen.ChunkProviderGenerate;

public class WorldTypeCLGen extends WorldType
{
	public WorldTypeCLGen(String name)
	{
		super(name);
	}
	
	/**
	 * Should world creation GUI show 'Customize' button for this world type?
	 * @return if this world type has customization parameters
	 */
	public boolean isCustomizable()
	{
		return true;
	}
	
	/**
	 * Called when the 'Customize' button is pressed on world creation GUI
	 * @param instance The minecraft instance
	 * @param guiCreateWorld the createworld GUI
	 */
	@SideOnly(Side.CLIENT)
	public void onCustomizeButton(Minecraft instance, GuiCreateWorld guiCreateWorld)
	{
		instance.displayGuiScreen(new GuiCreateCLGenWorld(guiCreateWorld, guiCreateWorld.field_146334_a));
	}
	
	public IChunkProvider getChunkGenerator(World world, String generatorOptions)
	{
		return new ChunkProviderCLGen(world, world.getSeed(), world.getWorldInfo().isMapFeaturesEnabled(), generatorOptions);
	}
}